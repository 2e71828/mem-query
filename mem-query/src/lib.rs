//! Memquery: In-memory relation manipulations
//! 
//! Memquery is an in-memory relational database engine that does its
//! query planning at compile time, which provides a zero-cost
//! abstraction for complex data manipulations.
//! 
//! All of the data storage backends provide the same API, so you
//! can adjust your data storage strategy for optimal performance
//! without a significant retooling of the domain logic that relies
//! on the data.
//!
//! Because of the amount of work this crate does with the type system,
//! you will probably need to raise the recursion limit in your crate
//! to 256 or higher:
//! ```
//! #![recursion_limit="256"]
//! ```

#![recursion_limit="256"]
#![feature(min_type_alias_impl_trait)]
#![cfg_attr(feature = "const", feature(const_generics, const_evaluatable_checked))]
#![allow(unused_imports)]

pub use tylisp;

#[macro_use] pub mod prelude;

#[cfg(test)]
#[macro_use] mod test_aids;

#[macro_use]
pub mod col;

pub mod rename_col;


pub mod header;
pub mod record;
pub mod relation;
pub mod query;
pub mod transaction;
pub(crate) mod range;
