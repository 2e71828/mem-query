#![allow(unused_macros)]

macro_rules! assert_type_eq {
    {$t1:ty : $t2:ty} => {let _: ::std::marker::PhantomData<$t2> = ::std::marker::PhantomData::<$t1>; }
}

macro_rules! assert_trait {
    {$t1:ty : $($trait:tt)+} => {
        {
            fn test<T:$($trait)+>(_: ::std::marker::PhantomData<T>) {}
            test(::std::marker::PhantomData::<$t1>);
        }
    }
}

macro_rules! assert_val_type {
    {$e:ident : $t2:ty} => {let _: & $t2 = &($e); }
}

macro_rules! assert_val_trait {
    {$e:ident : $($trait:tt)+} => {
        {
            fn test<T:$($trait)+>(_: &T) {}
            test(&($e));
        }
    }
}
