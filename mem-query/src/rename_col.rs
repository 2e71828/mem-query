use crate::col::Col;
use crate::tylisp::{HCons,HNil};

pub trait RenameCol<A:Col, B:Col<Inner = A::Inner>> {
    type Renamed;
    fn rename_col(self)->Self::Renamed;
}

impl<A:Col,B:Col<Inner = A::Inner>> RenameCol<A,B> for HNil {
    type Renamed = HNil;
    fn rename_col(self)->HNil { HNil }
}

impl<H,T,A:Col,B:Col<Inner = A::Inner>> RenameCol<A,B> for HCons<H,T>
where
    H: RenameCol<A,B>,
    T: RenameCol<A,B>
{
    type Renamed = HCons<H::Renamed, T::Renamed>;
    fn rename_col(self)->Self::Renamed { HCons {
        head: self.head.rename_col(),
        tail: self.tail.rename_col()
    }}
}


#[test]
fn test_rename_col() {
    use crate::tylisp::sexpr;
    col!{A:usize};
    col!{B:usize};
    col!{C:usize};

    assert_type_eq!{ <A as RenameCol<B,C>>::Renamed : A };
    assert_type_eq!{ <B as RenameCol<B,C>>::Renamed : C };

    assert_type_eq!{ <sexpr!{A,B} as RenameCol<B,C>>::Renamed: sexpr!{A,C}};
}
