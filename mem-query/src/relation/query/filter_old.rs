use crate::{
    relation::{RelationImpl,Queryable,QueryOutput},
    query::{
        QueryPlanImpl,
        request::{QueryRequest, NewRequest},
        filter::{QueryFilter, FilterForHeader},
    }
};

use tylisp::{
    sexpr, sexpr_quoted_types, calc, defun, HNil,
    engine::Calc,
    marker_traits::{Pass, List},
    ops::{
        Phantom, Ret,
        list::{Concat},
    },
};

use std::marker::PhantomData;

pub struct FilterRel<Rel, Filt> {
    pub(crate) source: Rel,
    pub(crate) filter: Filt,
}

impl<Rel, Filt> RelationImpl for FilterRel<Rel,Filt>
where
    Rel: RelationImpl,
    Filt: QueryFilter + List,
    sexpr!{FilterForHeader, @Rel::Cols, @Filt}: Pass
{
    type Cols = Rel::Cols;
    type FastCols = Rel::FastCols;
    type Planner = Planner;
}

impl<'a, Rel, Filt> QueryOutput<'a> for FilterRel<Rel,Filt>
where
    Rel: QueryOutput<'a>,
    Self: RelationImpl<Cols = Rel::Cols>,
{
    type QueryRow = Rel::QueryRow;
}

#[derive(Default,Copy,Clone,Debug)]
pub struct IterAllQuery;
defun!{ IterAllQuery {
    (Src: RelationImpl, Filter: QueryFilter) {_:PhantomData<Src>, filt: Filter} =>
        {NewRequest, @Filter = filt,
                     {Phantom, @HNil}};
}}

impl<'a,R,F,Q> IntoIterator for &'a FilterRel<R,F>
where sexpr!{IterAllQuery, {Phantom, @R}, @F=f}: Calc<sexpr_quoted_types!{IterAllQuery, {Phantom, @R}, @F=f}, Result=Q>,
    F: QueryFilter,
    Q: QueryRequest + 'a,
    R: Queryable<'a, Q>,
{
    type Item = R::QueryRow;
    type IntoIter = <R::Plan as IntoIterator>::IntoIter;
    fn into_iter(self)->Self::IntoIter {
        self.source.query(
            calc!{IterAllQuery, {Phantom, @R}, @F=self.filter.clone()}
        )
    }
}

#[derive(Default,Copy,Clone,Debug)]
pub struct BuildPlan;
defun!{ BuildPlan {
    ('a, Rel, Req) {source: &'a Rel, req: Req}
    => {Ret, @FilterRelPlan<'a, Rel, Req> = FilterRelPlan { source, req }};
}}

#[derive(Default,Copy,Clone,Debug)]
pub struct Planner;
defun!{ Planner {
    ('a, Rel, Filt: QueryFilter + Clone, Req: QueryRequest) {fr: &'a FilterRel<Rel, Filt>, req: Req} =>
        {BuildPlan, @&'a Rel = &fr.source,
                    {Concat, @Filt = fr.filter.clone(),
                             @Req::Filters = req.into_filters()},
                    {Phantom, @Req::OrderBy}};
}}

pub struct FilterRelPlan<'a, Rel, Req> {
    source: &'a Rel,
    req: Req,
}

impl<'a, Rel, Req> IntoIterator for FilterRelPlan<'a, Rel, Req>
where
    Req: QueryRequest + 'a,
    Rel: Queryable<'a, Req>,
{
    type Item = Rel::QueryRow;
    type IntoIter = impl Iterator<Item = Self::Item>;
    fn into_iter(self)->Self::IntoIter {
        self.source.query(self.req)
    }
}

impl<
    'orig, OrigRel:'orig, OrigReq,
    Rel, Req
> QueryPlanImpl<'orig, OrigRel, OrigReq> for FilterRelPlan<'orig,Rel, Req>
where
    sexpr!{Planner, @&'orig OrigRel = rel, @OrigReq = req}:
        Calc<sexpr_quoted_types!{Planner, @&'orig OrigRel = rel, @OrigReq = req},
             Result = Self>
{
    fn prepare(rel: &'orig OrigRel, req: OrigReq)->Self {
        calc!{Planner, @&'orig OrigRel = rel, @OrigReq = req}
    }
}

#[test]
fn test_filter_rel() {
    col!{A: usize}
    col!{B: usize}

    use crate::relation::{Relation,OpaqueRel};
    use crate::record::Record;
    let source: OpaqueRel<Vec<(A,B)>> = OpaqueRel::new(vec![ (A(3), B(6)), (A(5), B(2)), (A(3), B(7)) ]);

    let result: Vec<usize> = source.as_ref().where_eq(A(3)).into_iter().map(|r| **r.col_ref::<B>()).collect();
    assert_eq!(vec![6usize,7], result);

    let result: Vec<usize> = source.where_eq(A(3)).into_iter().map(|r| **r.col_ref::<B>()).collect();
    assert_eq!(vec![6usize,7], result);
}

#[test]
fn test_filter_rel_ref() {
    col!{A: usize}
    col!{B: usize}

    use crate::relation::{Relation,OpaqueRel};
    use crate::record::Record;
    let source: OpaqueRel<Vec<(A,B)>> = OpaqueRel::new(vec![ (A(3), B(6)), (A(5), B(2)), (A(3), B(7)) ]);

    let result: Vec<usize> = source.as_ref().where_eq_ref(&A(3)).into_iter().map(|r| **r.col_ref::<B>()).collect();
    assert_eq!(vec![6usize,7], result);

    let result: Vec<usize> = source.where_eq_ref(&A(3)).into_iter().map(|r| **r.col_ref::<B>()).collect();
    assert_eq!(vec![6usize,7], result);
}
