use crate::{
    header::Header,
    query::filter::QueryFilter,
    relation::RelationImpl,
    record::Record,
    transaction::{Transaction, RevertableOp},
};

use std::convert::Infallible;
use either::Either;

pub trait Delete<Q:QueryFilter> {
    type Op: RevertableOp<Self>;
    type Deleter: Fn(Q)->Self::Op;

    fn deleter() -> Self::Deleter;
    fn delete_op(q:Q)->Self::Op {
        (Self::deleter())(q)
    }
}
