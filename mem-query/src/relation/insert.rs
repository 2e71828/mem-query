use crate::{
    header::Header,
    relation::RelationImpl,
    record::Record,
    transaction::{Transaction, RevertableOp},
};

use std::convert::Infallible;
use either::Either;

pub trait Insert<H:Header>: RelationImpl {
    /// The fields from `H` that aren't consumed
    type Remainder: Sized;
    type Op: RevertableOp<Self>;

    fn insert_op<FromRec:Record<Cols=H>>(rec:FromRec) -> (Self::Op, Self::Remainder);
    fn insert<FromRec:Record<Cols=H>>(&mut self, rec:FromRec)
    ->Result<(), Either<<Self::Op as RevertableOp<Self>>::Err, Infallible>> {
        let (op, _) = Self::insert_op(rec);
        Transaction::start(self).apply(op).commit()
    }

    fn insert_multi(&mut self, recs:impl IntoIterator<Item=impl Record<Cols=H>>)
    ->Result<(), Either<<Self::Op as RevertableOp<Self>>::Err, Infallible>> {
        Transaction::start(self)
            .apply_multi(recs.into_iter().map(|rec| Self::insert_op(rec).0))
            .commit()
    }
}
