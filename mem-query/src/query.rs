use crate::{
    relation::{Relation,QueryOutput},
};

pub mod fallback;
pub mod filter;
pub mod order;
pub mod request;

use request::*;

/// A query result iterator
///
/// This is a wrapper around `IntoIterator` that verifies the returned items
/// are records that have an appropriate lifetime and header.

/*
pub trait RealizedQuery<'a, Cols:Header> {
    type Iter: Iterator<Item = Self::Row>;
    type Row: ExternalRecord<'a, Cols = Cols>;
    fn execute(self)->Self::Iter;
}

impl<'a, Cols:Header, Q, Rec> RealizedQuery<'a, Cols> for Q
where Q:IntoIterator<Item = Rec>,
      Rec: ExternalRecord<'a, Cols = Cols> {
    type Iter = <Self as IntoIterator>::IntoIter;
    type Row = <Self as IntoIterator>::Item;
    fn execute(self)->Self::Iter { self.into_iter() }
}
*/

pub trait QueryPlanImpl<'a, Rel:'a, Req> {
    // type EstimatedCost;
    fn prepare(rel: &'a Rel, req: Req)->Self;
}

pub trait QueryPlan<'a, Rel:'a + QueryOutput<'a>, Req:QueryRequest>:
    QueryPlanImpl<'a, Rel, Req>
    + IntoIterator<Item = Rel::QueryRow>
    + Sized
{
    fn execute(self)->Self::IntoIter { self.into_iter() }
}

impl<'a, Rel, Req, QP> QueryPlan<'a, Rel, Req> for QP
where QP: QueryPlanImpl<'a, Rel, Req>
        + IntoIterator<Item = Rel::QueryRow>,
      Rel: 'a + QueryOutput<'a>,
      Req: QueryRequest,
{}

