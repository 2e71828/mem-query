use std::{
    marker::PhantomData,
};
use crate::{
    header::{Header, ProjectFrom, HasCol},
    record::{Record, Col, ExternalRecord}
};

pub struct Projection<I,C> {
    inner: I,
    cols: PhantomData<C>
}

impl<I,C> Copy for Projection<I,C> where I:Copy {}
impl<I,C> Clone for Projection<I,C> where I:Clone {
    fn clone(&self)->Self {
        Projection { inner: self.inner.clone(), cols: PhantomData }
    }
}

impl<I,C:Header> Projection<I,C> {
    pub fn new(inner: I)->Self
    where I: Clone + Record, C: ProjectFrom<I::Cols> {
        Self { inner, cols: PhantomData }
    }

    pub fn new_unchecked(inner: I)->Self {
        Self { inner, cols: PhantomData }
    }
}

impl<I,Cols:Header> Record for Projection<I,Cols>
where I: Record
{
    type Cols = Cols;

    #[inline(always)]
    fn into_cols(self)->Cols {
        self.clone_cols()
    }

    #[inline(always)]
    fn clone_cols<'a>(&'a self)->Cols {
        Cols::clone_from_rec_unchecked(&self.inner)
    }

    #[inline(always)]
    fn col_ref<C:Col>(&self)->&C where Self::Cols: HasCol<C> {
        // Safety: If C is in the projection, it must also be in the source record.
        //         Therefore, this will never return `None`
        match self.inner.col_opt() {
            Some(c) => c,
            None => unreachable!()
        }
    }

    #[inline(always)]
    fn col_opt<C:Col>(&self)->Option<&C> {
        if Cols::has_col::<C>() {
            self.inner.col_opt()
        } else {
            None
        }
    }
}

impl<'a, I, Cols:Header> ExternalRecord<'a> for Projection<I,Cols>
where I: ExternalRecord<'a> {
    #[inline(always)]
    fn ext_col_ref<C:Col>(&self)->&'a C where Self::Cols: HasCol<C> {
        // Safety: If C is in the projection, it must also be in the source record.
        //         Therefore, this will never return `None`
        match self.inner.ext_col_opt() {
            Some(c) => c,
            None => unreachable!()
        }
    }

    #[inline(always)]
    fn ext_col_opt<C:Col>(&self)->Option<&'a C> {
        if Cols::has_col::<C>() {
            self.inner.ext_col_opt()
        } else {
            None
        }
    }
}
