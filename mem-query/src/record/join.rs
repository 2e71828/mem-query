/*use tylisp::ops::{
    list::{Intersect}
};

pub struct Join<L,R>( pub L, pub R );

type JoinKey<L:Record, R:Record> = eval!{Intersect, @L::Cols, @R::Cols};
type RCols<L:Record, R:Record> = <JoinKey<L,R> as ProjectFrom<R::Cols>>::Remainder;
type Header<L:Record, R:Record> = calc_ty!{Concat, @L::Cols=l, @RCols<L,R>=r};


impl<L:Record, R:Record> Record for Join<L,R>
where
    JoinKey<L,R>: Header + ProjectFrom<L::Cols> + ProjectFrom<R::Cols>,
    RCols<L,R>: ProjectFrom<R::Cols>,

{
    type Cols = Header<L,R>;

    fn into_cols(self)->Self::Cols {
        calc!{
            Concat,
            @L::Cols=self.0.into_cols(),
            @RCols<L,R> = RCols::<L,R>::project_from(self.1.into_cols())
        }
    }

    fn clone_cols(&self)->Self::Cols {
        
    }

*/
