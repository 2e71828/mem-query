use ::mem_query as mq;
use mq::{
    prelude::*,
    relation::{BTreeIndex, Insert, RelationImpl, RedundantIndex},
};
use ::datagen::*;

#[derive(Default)]
pub struct Inventory {
    parts: BTreeIndex<PartId, Option<Part>>,
    projects: RedundantIndex<ProjectName, ProjectId,
                BTreeIndex<ProjectId, Option<Project>>>,
    commits: BTreeIndex<ProjectId, BTreeIndex<PartId, Option<Commit>>>,
}

impl Inventory {
    pub fn query_parts_by_project_name(
        &self,
        project: &str,
        out: &mut dyn FnMut(QueryResultRef)
    ) {
        self.projects.as_ref()
            .join(self.commits.as_ref())
            .join(self.parts.as_ref())
            .where_eq(&ProjectName(String::from(project)))
            .iter_as::<QueryResultRef>()
            .for_each(out)
    }

    pub fn load(&(ref projects, ref parts, ref commits): &(Vec<Project>, Vec<Part>, Vec<Commit>))->Self {
        let mut inv: Self = Default::default();
        inv.projects.insert_multi(projects.iter().cloned()).unwrap();
        inv.parts.insert_multi(parts.iter().cloned()).unwrap();
        let projects = &inv.projects;
        inv.commits.insert_multi(commits.iter().map(|c|
            (c.clone(), projects.as_ref().where_eq(ProjectId(c.project_id)).iter_as::<ProjectName>().next().unwrap())
        )).unwrap();
        inv
    }
}

