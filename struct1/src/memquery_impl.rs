use ::mem_query as mq;
use mq::{
    prelude::*,
    relation::{BTreeIndex, Insert, RelProxy, RelationImpl},
};
use ::datagen::*;

use std::rc::Rc;

mq::col!{ PartProjects: RelProxy<Rc<BTreeIndex<ProjectId, Option<(Project, QtyCommitted)>>>> }

#[derive(Default)]
pub struct Inventory {
    parts: BTreeIndex<PartId, Option<(Part, PartProjects)>>,
}

impl Inventory {
    pub fn query_parts_by_project_name(
        &self,
        project: &str,
        out: &mut dyn FnMut(QueryResultRef)
    ) {
        self.parts.as_ref().subjoin::<PartProjects>()
            .where_eq(&ProjectName(String::from(project)))
            .iter_as::<QueryResultRef>()
            .for_each(out)
    }

    pub fn load(&(ref projects, ref parts, ref commits): &(Vec<Project>, Vec<Part>, Vec<Commit>))->Self {
        use std::collections::BTreeMap;
        let projects: BTreeMap<usize, &Project> = projects.iter().map(|proj| (proj.id, proj)).collect();
        let mut commits_by_part: BTreeMap<usize, Vec<&Commit>> = Default::default();
        for commit in commits {
            commits_by_part.entry(commit.part_id).or_default().push(commit);
        }

        let mut inv: Self = Default::default();
        inv.parts.insert_multi(parts.iter().map(|part| {
            let mut part_projects:BTreeIndex<_,_> = Default::default();
            part_projects.insert_multi(
                commits_by_part.entry(part.id).or_default().iter().map(|&Commit{ project_id, quantity_committed, part_id: _ }| {
                    (projects[project_id].clone(), QtyCommitted(*quantity_committed))
                })
            ).unwrap();
            (part, PartProjects(RelProxy::new(part_projects)))
        })).unwrap();
        inv
    }
}
