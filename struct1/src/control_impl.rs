use datagen::QueryResultRef;
use datagen as data;
use std::collections::BTreeMap;

#[allow(dead_code)]
struct Part {
    id: usize,
    name: String,
    description: String,
    quantity_on_hand: usize,
    quantity_on_order: usize,
    projects: BTreeMap<usize, Project>,
}

#[allow(dead_code)]
struct Project {
    id: usize,
    name: String,
    description: String,
    quantity_committed: usize,
}

pub struct Inventory {
    parts: BTreeMap<usize, Part>,
}

impl Inventory {
    pub fn query_parts_by_project_name(
        &self,
        project: &str,
        out: &mut dyn FnMut(QueryResultRef)
    ) {
        self.parts.values().for_each(|part| {
            part.projects
                .values()
                .filter(|proj| proj.name == project)
                .for_each(|proj| out(QueryResultRef {
                    part_id: part.id,
                    part_name: &part.name,
                    quantity_committed: proj.quantity_committed,
                }))
        })
    }

    pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
        let mut result = Inventory { parts: BTreeMap::new() };
        for part in parts {
            result.parts.insert(part.id, Part {
                id: part.id,
                name: part.name.clone(),
                description: part.description.clone(),
                quantity_on_hand: part.quantity_on_hand,
                quantity_on_order: part.quantity_on_order,
                projects: BTreeMap::new()
            });
        }

        let projects: BTreeMap<usize, &data::Project> = projects.iter().map(|proj| (proj.id, proj)).collect();

        for commit in commits {
            let proj = projects[&commit.project_id];
            result.parts.get_mut(&commit.part_id).unwrap().projects.insert(commit.project_id, Project {
                id: proj.id,
                name: proj.name.clone(),
                description: proj.description.clone(),
                quantity_committed: commit.quantity_committed
            });
        }
        result
    }
}
