#![recursion_limit = "512"]

const NUM_SAMPLES: usize = 50;
const WARM_UP_TIME: u64 = 60; // secs
const MEASUREMENT_TIME: u64 = 600; // secs
#[macro_use] extern crate criterion;
use self::criterion::*;

criterion_group!(benches, my_bench);
criterion_main!(benches);

type Group<'a> = criterion::BenchmarkGroup<'a,criterion::measurement::WallTime>;

macro_rules! make_bench {
    ($data:ident => $($mod:tt)*) => {
        { 
            let inv = $($mod)*::Inventory::load(&$data.0);
            let queries = &$data.1;
//            let bb = Box::new(|x| { criterion::black_box(x); });
            let func:Box<dyn Fn(&mut Group)>
                = Box::new(move |group| { group.bench_function(
                    stringify!($data => $($mod)*),
                    |b| {
                        b.iter(|| queries.iter().for_each(
                            |q| (&inv).query_parts_by_project_name(q, &mut |x| { criterion::black_box(x); })
                        ));
                    }
                );});
            func
        }
    }
}

pub fn my_bench(c: &mut Criterion) {
    #[cfg(feature = "small")]
    let small = datagen::make_data(100, 1_000, 2_000, 100);

    #[cfg(feature = "medium")]
    let medium = datagen::make_data(1000, 10_000, 20_000, 100);

    #[cfg(feature = "large")]
    let large = datagen::make_data(10_000, 100_000, 200_000, 100);

    let mut tests:Vec<Box<dyn Fn(&mut Group)>> = vec![];

    macro_rules! register_tests {
        ($data:ident) => {
            #[cfg(feature = "struct1")] {
                tests.push(make_bench!($data => struct1::control_impl));
                tests.push(make_bench!($data => struct1::memquery_impl));
            }

            #[cfg(feature = "struct1_alt")] {
                tests.push(make_bench!($data => struct1_alt::control_impl));
                tests.push(make_bench!($data => struct1_alt::memquery_impl));
            };

            #[cfg(feature = "struct2")] {
                tests.push(make_bench!($data => struct2::control_impl));
                tests.push(make_bench!($data => struct2::memquery_impl));
            }

            #[cfg(feature = "struct2_alt")] {
                tests.push(make_bench!($data => struct2_alt::control_impl));
                tests.push(make_bench!($data => struct2_alt::memquery_impl));
            };

            #[cfg(feature = "struct3")] {
                tests.push(make_bench!($data => struct3::control_impl));
                tests.push(make_bench!($data => struct3::memquery_impl));
            }

            #[cfg(feature = "struct3_alt")] {
                tests.push(make_bench!($data => struct3_alt::control_impl));
                tests.push(make_bench!($data => struct3_alt::memquery_impl));
            };

            #[cfg(feature = "struct4")] {
                tests.push(make_bench!($data => struct4::control_impl));
                tests.push(make_bench!($data => struct4::memquery_impl));
            }

            #[cfg(feature = "struct4_alt")] {
                tests.push(make_bench!($data => struct4_alt::control_impl));
                tests.push(make_bench!($data => struct4_alt::memquery_impl));
            };

            #[cfg(feature = "struct5")] {
                tests.push(make_bench!($data => struct5::control_impl));
                tests.push(make_bench!($data => struct5::memquery_impl));
            }

            #[cfg(feature = "struct5_alt")] {
                tests.push(make_bench!($data => struct5_alt::control_impl));
                tests.push(make_bench!($data => struct5_alt::memquery_impl));
            };
        }
    }

    #[cfg(feature = "small")] register_tests!{small};
    #[cfg(feature = "medium")] register_tests!{medium};
    #[cfg(feature = "large")] register_tests!{large};

    {
        let plot_config = PlotConfiguration::default()
            .summary_scale(AxisScale::Logarithmic);

        let mut exp = c.benchmark_group("Forward");
        exp.sample_size(NUM_SAMPLES);
        exp.warm_up_time(std::time::Duration::from_secs(WARM_UP_TIME));
        exp.measurement_time(std::time::Duration::from_secs(MEASUREMENT_TIME));
        exp.plot_config(plot_config);

        tests.iter().for_each(|t| t(&mut exp));
    }

    #[cfg(feature = "double")] {
        let plot_config = PlotConfiguration::default()
            .summary_scale(AxisScale::Logarithmic);
        let mut exp = c.benchmark_group("Reverse");

        exp.sample_size(NUM_SAMPLES);
        exp.plot_config(plot_config);

        tests.iter().rev().for_each(|t| t(&mut exp));
    }
}
