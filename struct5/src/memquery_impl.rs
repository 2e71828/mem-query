use ::mem_query as mq;
use mq::{
    prelude::*,
    relation::{BTreeIndex, RedundantIndex, Insert, RelationImpl},
};
use ::datagen::*;

#[derive(Default)]
pub struct Inventory {
    parts: BTreeIndex<PartId, Option<Part>>,
    projects: BTreeIndex<ProjectId, Option<Project>>,
    commits: RedundantIndex<ProjectId, PartId,
                BTreeIndex<PartId, Vec<Commit>>>,
}

impl Inventory {
    pub fn query_parts_by_project_name(
        &self,
        project: &str,
        out: &mut dyn FnMut(QueryResultRef)
    ) {
        self.projects.as_ref()
            .join(self.commits.as_ref())
            .join(self.parts.as_ref())
            .where_eq(&ProjectName(String::from(project)))
            .iter_as::<QueryResultRef>()
            .for_each(out)
    }

    pub fn load(data: &(Vec<Project>, Vec<Part>, Vec<Commit>))->Self {
        let &(ref projects, ref parts, ref commits) = data;
        let mut inv: Self = Default::default();
        inv.projects.insert_multi(projects.iter().cloned()).unwrap();
        inv.parts.insert_multi(parts.iter().cloned()).unwrap();
        inv.commits.insert_multi(commits.iter().cloned()).unwrap();
        inv
    }
}

