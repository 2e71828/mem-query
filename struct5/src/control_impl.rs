use datagen::QueryResultRef;
use datagen as data;
use std::collections::BTreeMap;

#[allow(dead_code)]
struct Project {
    id: usize,
    name: String,
    description: String,
}

#[allow(dead_code)]
struct Part {
    id: usize,
    name: String,
    description: String,
    quantity_on_hand: usize,
    quantity_on_order: usize,
}

#[allow(dead_code)]
struct Commit {
    part_id: usize,
    project_id: usize,
    quantity_committed: usize,
}

pub struct Inventory {
    parts: BTreeMap<usize, Part>,
    projects: BTreeMap<usize, Project>,
    commitments: BTreeMap<(usize, usize), Commit>, // (part_id, project_id)
}

impl Inventory {
    pub fn query_parts_by_project_name<'a>(
        &'a self,
        project: &'a str,
        out: &mut dyn FnMut(QueryResultRef)
    ) {
        self.projects
            .values()
            .filter(|proj| proj.name == project)
            .for_each(|proj| {
                self.commitments
                    .values()
                    .filter(|commit| commit.project_id == proj.id)
                    .for_each(|commit| out(QueryResultRef {
                        part_id: commit.part_id,
                        part_name: &self.parts[&commit.part_id].name,
                        quantity_committed: commit.quantity_committed,
                    }))
            })
    }

    pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
        let mut result = Inventory {
            parts: BTreeMap::new(),
            projects: BTreeMap::new(),
            commitments: BTreeMap::new()
        };
        //let proj_names: BTreeMap<usize, String> = projects.iter().map(|proj| (proj.id, proj.name.clone())).collect();

        result.parts.extend(parts.iter().map(|part| (part.id, Part {
            id: part.id,
            name: part.name.clone(),
            description: part.description.clone(),
            quantity_on_hand: part.quantity_on_hand,
            quantity_on_order: part.quantity_on_order,
        })));
        
        //result.projects.extend(projects.iter().map(|proj| (proj.name.clone(), Project {
        result.projects.extend(projects.iter().map(|proj| (proj.id, Project {
            id: proj.id,
            name: proj.name.clone(),
            description: proj.description.clone(),
        })));

        result.commitments.extend(commits.iter().map(|commit| ((commit.part_id, commit.project_id), Commit {
            part_id: commit.part_id,
            project_id: commit.project_id,
            quantity_committed: commit.quantity_committed
        })));

        result
    }
}
