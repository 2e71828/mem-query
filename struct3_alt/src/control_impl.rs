use datagen::QueryResultRef;
use datagen as data;
use std::collections::BTreeMap;

#[allow(dead_code)]
struct Project {
    id: usize,
    name: String,
    description: String,
    parts_committed: BTreeMap<usize, usize>,
}

#[allow(dead_code)]
struct Part {
    id: usize,
    name: String,
    description: String,
    quantity_on_hand: usize,
    quantity_on_order: usize,
}

pub struct Inventory {
    parts: BTreeMap<usize, Part>,
    projects: BTreeMap<String, Project>,
}

impl Inventory {
    pub fn query_parts_by_project_name<'a>(
        &'a self,
        project: &'a str,
        out: &mut dyn Fn(QueryResultRef)
    ) {
        self.projects
            .get(project)
            .into_iter()
            .for_each(|proj| {
                proj.parts_committed
                    .iter()
                    .for_each(|(&part_id, &quantity_committed)| out(QueryResultRef {
                        part_id,
                        part_name: &self.parts[&part_id].name,
                        quantity_committed,
                    }))
            })
    }

    pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
        let mut result = Inventory {
            parts: BTreeMap::new(),
            projects: BTreeMap::new(),
        };
        let proj_names: BTreeMap<usize, String> = projects.iter().map(|proj| (proj.id, proj.name.clone())).collect();

        result.parts.extend(parts.iter().map(|part| (part.id, Part {
            id: part.id,
            name: part.name.clone(),
            description: part.description.clone(),
            quantity_on_hand: part.quantity_on_hand,
            quantity_on_order: part.quantity_on_order,
        })));
        
        result.projects.extend(projects.iter().map(|proj| (proj.name.clone(), Project {
            id: proj.id,
            name: proj.name.clone(),
            description: proj.description.clone(),
            parts_committed: BTreeMap::new(),
        })));

        for commit in commits {
            result.projects.get_mut(&proj_names[&commit.project_id]).unwrap()
                  .parts_committed.insert(commit.part_id, commit.quantity_committed);
        }

        result
    }
}
