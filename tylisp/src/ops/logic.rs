use typenum::{True,False};
use super::{Ret,If};

#[derive(Debug,Default)]
pub struct And;
defmacro!{ And {
    (   ) { } => {Ret, @True};
    (H,T) { H; T } => {If, H, {And; T}, @False};
}}

#[derive(Debug,Default)]
pub struct Or;
defmacro!{ Or {
    (H,T) { H; T } => {If, H, @True, {Or; T}};
    (   ) { } => {Ret, @False};
}}

#[derive(Debug,Default)]
pub struct Not;
defun!{ Not {
    () { _:True } => {Ret, @False = Default::default()};
    () { _:False } => {Ret, @True = Default::default()};
}}

#[derive(Debug,Default)]
pub struct Invert;

defun_nocalc!{() Invert {
    (Pred) { _:Pred } => {Ret, @Inverted<Pred>};
}}

#[derive(Debug,Default)]
pub struct Inverted<P>(P);

defun_nocalc!{(P) Inverted<P> {
    (P, Args) { ; _:Args } => {Not, {@P; Args}};
}}

#[derive(Debug,Default)]
pub struct Yes;
defun_nocalc!{() Yes {
    (Args) {; _:Args} => {Ret, @True};
}}

#[derive(Debug,Default)]
pub struct No;
defun_nocalc!{() No {
    (Args) {; _:Args} => {Ret, @False};
}}
