use crate::ops::*;
pub use std::rc::Rc;
pub use std::cell::Cell;

#[derive(Debug,Default)]
pub struct WrapRc;
defun!{ WrapRc {
    (T) { t:T } => {Ret, @Rc<T> = Rc::new(t)};
}}

#[derive(Debug,Default)]
pub struct UnwrapRc;
defun!{ UnwrapRc {
    (T) { rc:Rc<T> } => {
        Ret, @T = Rc::try_unwrap(rc)
                     .unwrap_or_else(|_| panic!("Couldn't unwrap {}",
                                                core::any::type_name::<T>()))
    };
}}

#[derive(Debug,Default)]
pub struct Unwrap;
defun!{ Unwrap {
    (T) { opt:Option<T> } => {
        Ret, @T = opt.unwrap_or_else(|| panic!("Couldn't unwrap {}",
                                                core::any::type_name::<T>()))
    };
}}
