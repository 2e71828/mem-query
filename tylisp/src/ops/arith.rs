use typenum as tn;
use crate::ops::{Ret};

#[derive(Debug,Default)]
pub struct Add;
defun!{ Add{
    (L: std::ops::Add<R>, R) {l:L, r:R} => {Ret, @L::Output = l+r};
}}

#[derive(Debug,Default)]
pub struct Inc;
defun_nocalc!{() Inc{
    (L: std::ops::Add<tn::U1>) {l:L} => {Ret, @L::Output};
}}

#[test]
fn test_add() {
    assert_type_eq!(eval!{Add, tn::U4, tn::U3}, tn::U7);
    assert_type_eq!(eval!{Inc, tn::U4}, tn::U5);
    assert_type_eq!(eval!{Add, @u32, @u32}, u32);
    assert_eq!(7, calc!{Add, @u32 = 3, @u32 = 4});
}

#[derive(Debug,Default)]
pub struct Sub;
defun!{ Sub{
    (L: std::ops::Sub<R>, R) {l:L, r:R} => {Ret, @L::Output = l-r};
}}

#[derive(Debug,Default)]
pub struct Dec;
defun_nocalc!{() Dec{
    (L: std::ops::Sub<tn::U1>) {l:L} => {Ret, @L::Output};
}}

#[test]
fn test_sub() {
    assert_type_eq!(eval!{Sub, tn::U4, tn::U3}, tn::U1);
    assert_type_eq!(eval!{Dec, tn::U4}, tn::U3);
    assert_type_eq!(eval!{Sub, @u32, @u32}, u32);
    assert_eq!(1, calc!{Sub, @u32 = 4, @u32 = 3});
}
