#![recursion_limit = "256"]
use tylisp as tl;

#[cfg(test)]
macro_rules! assert_type_eq {
    ($a:ty, $b:ty) => { let _: ::core::marker::PhantomData<$a>=
        <::core::marker::PhantomData<$b> as ::core::default::Default>::default();
    }
}

//type Test = sexpr!{ };
//type Test = sexpr!{ tn::True };
//type Test = eval!{ sexpr!{If, tn::False, &'static str, <u32>}};

#[test]
fn main() {
    use tl::{eval, ops::list::{Head,Tail,Reverse}};
    struct A;
    struct B;
    struct C;
    struct D;
    assert_type_eq!{ C, eval!{ Head, { Tail, { Reverse, @{A,B,C,D}}}}};
}

#[test] 
fn defun_rust() {
    use tl::{defun_rust, ops::{If}};
    defun_rust!{
        (fn cond<Bool,A,B>(a:A, b:B)) -> {If, Bool, @A=a, @B=b}
    };

    assert_eq!(cond::<typenum::True,_,_>(4, "hello"), 4);
    assert_eq!(cond::<typenum::False,_,_>(4, "hello"), "hello");
}

#[test]
fn test_is() {
    use tl::{eval, literal, ops::Is};
    #[derive(Default,Copy,Clone)] struct A;
    #[derive(Default,Copy,Clone)] struct B;
    literal!{A;B};
    assert_type_eq!{ typenum::True, eval!{Is, @A, @A}};
    assert_type_eq!{ typenum::False, eval!{Is, @A, @B}};
}
