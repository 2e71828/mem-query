use rand_chacha;
use rand;
use rand::{Rng, seq::SliceRandom};
use std::collections::BTreeMap;

#[derive(Clone, Debug)]
pub struct Project {
    pub id: usize,
    pub name: String,
    pub description: String,
}

#[derive(Clone, Debug)]
pub struct Part {
    pub id: usize,
    pub name: String,
    pub description: String,
    pub quantity_on_hand: usize,
    pub quantity_on_order: usize,
}

#[derive(Clone, Debug)]
pub struct Commit {
    pub part_id: usize,
    pub project_id: usize,
    pub quantity_committed: usize,
}

pub fn new_rng()->impl rand::Rng {
    // First 256 bits of pi, courtesy OEIS Seq. A062964
    const SEED:[u8;32] = [
        0x32, 0x43, 0xF6, 0xA8, 0x88, 0x5A, 0x30, 0x8D,
        0x31, 0x31, 0x98, 0xA2, 0xE0, 0x37, 0x07, 0x34,
        0x4A, 0x40, 0x93, 0x82, 0x22, 0x99, 0xF3, 0x1D,
        0x00, 0x82, 0xEF, 0xA9, 0x8E, 0xC4, 0xE6, 0xC8,
    ];

    <rand_chacha::ChaCha12Rng as rand::SeedableRng>::from_seed(SEED)
}

pub fn make_data(n_proj:usize, n_part:usize, n_commit:usize, n_queries:usize)
    ->((Vec<Project>, Vec<Part>, Vec<Commit>), Vec<String>) {
    let mut rng = new_rng();

    let mut projects: Vec<Project> = (0..n_proj).map(|id|
        Project {
            id,
            name: format!("proj-{:06}", id),
            description: format!("Project #{}", id)
        }
    ).collect();

    let mut parts: Vec<Part> = (0..n_part).map(|id|
        Part {
            id,
            name: format!("part-{:06}", id),
            description: format!("Part #{}", id),
            quantity_on_hand: rng.gen_range(0..100000),
            quantity_on_order: rng.gen_range(0..10000),
        }
    ).collect();

    let mut commits: BTreeMap<(usize,usize),usize> = BTreeMap::new();

    for _ in 0..n_commit {
        let proj = rng.gen_range(0..n_proj);
        let part = rng.gen_range(0..n_part);

        *commits.entry((proj,part)).or_default() += rng.gen_range(1..1000);
    }

    let mut commits: Vec<Commit> = commits.into_iter()
        .map(|((project_id, part_id), quantity_committed)| Commit {
            project_id, part_id, quantity_committed
        }).collect();

    projects.shuffle(&mut rng);
    parts.shuffle(&mut rng);
    commits.shuffle(&mut rng);
    
    let queries: Vec<String> = projects
        .choose_multiple(&mut rng, n_queries)
        .map(|proj| proj.name.clone())
        .collect();

    ((projects, parts, commits), queries)
} 
