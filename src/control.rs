use super::QueryResultRef;

/// Projects subordinate to parts
pub mod struct1 {
    use super::QueryResultRef;
    use std::collections::BTreeMap;
    use crate::datagen as data;

    #[allow(dead_code)]
    struct Part {
        id: usize,
        name: String,
        description: String,
        quantity_on_hand: usize,
        quantity_on_order: usize,
        projects: BTreeMap<usize, Project>,
    }

    #[allow(dead_code)]
    struct Project {
        id: usize,
        name: String,
        description: String,
        quantity_committed: usize,
    }

    pub struct Inventory {
        parts: BTreeMap<usize, Part>,
    }

    impl Inventory {
        pub fn query_parts_by_project_name(
            &self,
            project: &str,
            mut out: Box<dyn FnMut(QueryResultRef)>
        ) {
            self.parts.values().for_each(|part| {
                part.projects
                    .values()
                    .filter(|proj| proj.name == project)
                    .for_each(|proj| out(QueryResultRef {
                        part_id: part.id,
                        part_name: &part.name,
                        quantity_committed: proj.quantity_committed,
                    }))
            })
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            let mut result = Inventory { parts: BTreeMap::new() };
            for part in parts {
                result.parts.insert(part.id, Part {
                    id: part.id,
                    name: part.name.clone(),
                    description: part.description.clone(),
                    quantity_on_hand: part.quantity_on_hand,
                    quantity_on_order: part.quantity_on_order,
                    projects: BTreeMap::new()
                });
            }

            let projects: BTreeMap<usize, &data::Project> = projects.iter().map(|proj| (proj.id, proj)).collect();

            for commit in commits {
                let proj = projects[&commit.project_id];
                result.parts.get_mut(&commit.part_id).unwrap().projects.insert(commit.project_id, Project {
                    id: proj.id,
                    name: proj.name.clone(),
                    description: proj.description.clone(),
                    quantity_committed: commit.quantity_committed
                });
            }
            result
        }
    }
}

/// Projects subordinate to parts
pub mod struct1_alt {
    use super::QueryResultRef;
    use std::collections::BTreeMap;
    use crate::datagen as data;

    #[allow(dead_code)]
    struct Part {
        id: usize,
        name: String,
        description: String,
        quantity_on_hand: usize,
        quantity_on_order: usize,
        projects: BTreeMap<String, Project>,
    }

    #[allow(dead_code)]
    struct Project {
        id: usize,
        name: String,
        description: String,
        quantity_committed: usize,
    }

    pub struct Inventory {
        parts: BTreeMap<usize, Part>,
    }

    impl Inventory {
        pub fn query_parts_by_project_name(
            &self,
            project: &str,
            mut out: Box<dyn FnMut(QueryResultRef)>
        ) {
            self.parts.values().for_each(|part| {
                part.projects.get(project).map(|proj| out(QueryResultRef {
                    part_id: part.id,
                    part_name: &part.name,
                    quantity_committed: proj.quantity_committed,
                }));
            })
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            let mut result = Inventory { parts: BTreeMap::new() };
            for part in parts {
                result.parts.insert(part.id, Part {
                    id: part.id,
                    name: part.name.clone(),
                    description: part.description.clone(),
                    quantity_on_hand: part.quantity_on_hand,
                    quantity_on_order: part.quantity_on_order,
                    projects: BTreeMap::new()
                });
            }

            let projects: BTreeMap<usize, &data::Project> = projects.iter().map(|proj| (proj.id, proj)).collect();

            for commit in commits {
                let proj = projects[&commit.project_id];
                result.parts.get_mut(&commit.part_id).unwrap().projects.insert(proj.name.clone(), Project {
                    id: proj.id,
                    name: proj.name.clone(),
                    description: proj.description.clone(),
                    quantity_committed: commit.quantity_committed
                });
            }
            result
        }
    }
}

/// Parts subordinate to projects
pub mod struct2 {
    use super::QueryResultRef;
    use std::collections::BTreeMap;
    use crate::datagen as data;

    #[allow(dead_code)]
    struct Project {
        id: usize,
        name: String,
        description: String,
        parts: BTreeMap<usize, Part>,
    }

    #[allow(dead_code)]
    struct Part {
        id: usize,
        name: String,
        description: String,
        quantity_on_hand: usize,
        quantity_on_order: usize,
        quantity_committed: usize,
    }

    pub struct Inventory {
        projects: BTreeMap<usize, Project>,
    }

    impl Inventory {
        pub fn query_parts_by_project_name(
            &self,
            project: &str,
            mut out: Box<dyn FnMut(QueryResultRef)>
        ) {
            self.projects
                .values()
                .filter(move |proj| proj.name == project)
                .for_each(move |proj| {
                    proj.parts.values().for_each(|part| out(QueryResultRef {
                        part_id: part.id,
                        part_name: &part.name,
                        quantity_committed: part.quantity_committed,
                    }));
                });
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            let mut result = Inventory { projects: BTreeMap::new() };
            for proj in projects {
                result.projects.insert(proj.id, Project {
                    id: proj.id,
                    name: proj.name.clone(),
                    description: proj.description.clone(),
                    parts: BTreeMap::new()
                });
            }

            let parts: BTreeMap<usize, &data::Part> = parts.iter().map(|part| (part.id, part)).collect();

            for commit in commits {
                let part = parts[&commit.part_id];
                result.projects.get_mut(&commit.project_id).unwrap().parts.insert(part.id, Part {
                    id: part.id,
                    name: part.name.clone(),
                    description: part.description.clone(),
                    quantity_on_hand: part.quantity_on_hand,
                    quantity_on_order: part.quantity_on_order,
                    quantity_committed: commit.quantity_committed
                });
            }
            result
        }
    }
}

/// Parts subordinate to projects
pub mod struct2_alt {
    use super::QueryResultRef;
    use std::collections::BTreeMap;
    use crate::datagen as data;

    #[allow(dead_code)]
    struct Project {
        id: usize,
        name: String,
        description: String,
        parts: BTreeMap<usize, Part>,
    }

    #[allow(dead_code)]
    struct Part {
        id: usize,
        name: String,
        description: String,
        quantity_on_hand: usize,
        quantity_on_order: usize,
        quantity_committed: usize,
    }

    pub struct Inventory {
        projects: BTreeMap<String, Project>,
    }

    impl Inventory {
        pub fn query_parts_by_project_name(
            &self,
            project: &str,
            mut out: Box<dyn FnMut(QueryResultRef)>
        ) {
            self.projects
                .get(project)
                .into_iter()
                .for_each(move |proj| {
                    proj.parts.values().for_each(|part| out(QueryResultRef {
                        part_id: part.id,
                        part_name: &part.name,
                        quantity_committed: part.quantity_committed,
                    }));
                });
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            let mut result = Inventory { projects: BTreeMap::new() };
            let mut project_names: BTreeMap<usize, String> = BTreeMap::new();

            for proj in projects {
                result.projects.insert(proj.name.clone(), Project {
                    id: proj.id,
                    name: proj.name.clone(),
                    description: proj.description.clone(),
                    parts: BTreeMap::new()
                });

                project_names.insert(proj.id, proj.name.clone());
            }

            let parts: BTreeMap<usize, &data::Part> = parts.iter().map(|part| (part.id, part)).collect();

            for commit in commits {
                let part = parts[&commit.part_id];
                result.projects.get_mut(&project_names[&commit.project_id]).unwrap().parts.insert(part.id, Part {
                    id: part.id,
                    name: part.name.clone(),
                    description: part.description.clone(),
                    quantity_on_hand: part.quantity_on_hand,
                    quantity_on_order: part.quantity_on_order,
                    quantity_committed: commit.quantity_committed
                });
            }
            result
        }
    }
}

/// Parts and projects as peers
/// Commitment relationship subordinate to projects
pub mod struct3 {
    use super::QueryResultRef;
    use std::collections::BTreeMap;
    use crate::datagen as data;

    #[allow(dead_code)]
    struct Project {
        id: usize,
        name: String,
        description: String,
        parts_committed: BTreeMap<usize, usize>,
    }

    #[allow(dead_code)]
    struct Part {
        id: usize,
        name: String,
        description: String,
        quantity_on_hand: usize,
        quantity_on_order: usize,
    }

    pub struct Inventory {
        parts: BTreeMap<usize, Part>,
        projects: BTreeMap<usize, Project>,
    }

    impl Inventory {
        pub fn query_parts_by_project_name<'a>(
            &'a self,
            project: &'a str,
        ) -> impl Iterator<Item = QueryResultRef<'a>> + 'a {
            self.projects
                .values()
                .filter(move |proj| proj.name == project)
                .flat_map(move |proj| {
                    proj.parts_committed
                        .iter()
                        .map(move |(&part_id, &quantity_committed)| QueryResultRef {
                            part_id,
                            part_name: &self.parts[&part_id].name,
                            quantity_committed,
                        })
                })
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            let mut result = Inventory {
                parts: BTreeMap::new(),
                projects: BTreeMap::new(),
            };

            result.parts.extend(parts.iter().map(|part| (part.id, Part {
                id: part.id,
                name: part.name.clone(),
                description: part.description.clone(),
                quantity_on_hand: part.quantity_on_hand,
                quantity_on_order: part.quantity_on_order,
            })));
            
            result.projects.extend(projects.iter().map(|proj| (proj.id, Project {
                id: proj.id,
                name: proj.name.clone(),
                description: proj.description.clone(),
                parts_committed: BTreeMap::new(),
            })));

            for commit in commits {
                result.projects.get_mut(&commit.project_id).unwrap()
                      .parts_committed.insert(commit.part_id, commit.quantity_committed);
            }

            result
        }
    }
}

/// Parts and projects as peers
/// Commitment relationship subordinate to projects
pub mod struct3_alt {
    use super::QueryResultRef;
    use std::collections::BTreeMap;
    use crate::datagen as data;

    #[allow(dead_code)]
    struct Project {
        id: usize,
        name: String,
        description: String,
        parts_committed: BTreeMap<usize, usize>,
    }

    #[allow(dead_code)]
    struct Part {
        id: usize,
        name: String,
        description: String,
        quantity_on_hand: usize,
        quantity_on_order: usize,
    }

    pub struct Inventory {
        parts: BTreeMap<usize, Part>,
        projects: BTreeMap<String, Project>,
    }

    impl Inventory {
        pub fn query_parts_by_project_name<'a>(
            &'a self,
            project: &'a str,
        ) -> impl Iterator<Item = QueryResultRef<'a>> + 'a {
            self.projects
                .get(project)
                .into_iter()
                .flat_map(move |proj| {
                    proj.parts_committed
                        .iter()
                        .map(move |(&part_id, &quantity_committed)| QueryResultRef {
                            part_id,
                            part_name: &self.parts[&part_id].name,
                            quantity_committed,
                        })
                })
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            let mut result = Inventory {
                parts: BTreeMap::new(),
                projects: BTreeMap::new(),
            };
            let proj_names: BTreeMap<usize, String> = projects.iter().map(|proj| (proj.id, proj.name.clone())).collect();

            result.parts.extend(parts.iter().map(|part| (part.id, Part {
                id: part.id,
                name: part.name.clone(),
                description: part.description.clone(),
                quantity_on_hand: part.quantity_on_hand,
                quantity_on_order: part.quantity_on_order,
            })));
            
            result.projects.extend(projects.iter().map(|proj| (proj.name.clone(), Project {
                id: proj.id,
                name: proj.name.clone(),
                description: proj.description.clone(),
                parts_committed: BTreeMap::new(),
            })));

            for commit in commits {
                result.projects.get_mut(&proj_names[&commit.project_id]).unwrap()
                      .parts_committed.insert(commit.part_id, commit.quantity_committed);
            }

            result
        }
    }
}

/// Parts and projects as peers
/// Commitment relationship subordinate to parts
pub mod struct4 {
    use super::QueryResultRef;
    use std::collections::BTreeMap;
    use crate::datagen as data;

    #[allow(dead_code)]
    struct Project {
        id: usize,
        name: String,
        description: String,
    }

    #[allow(dead_code)]
    struct Part {
        id: usize,
        name: String,
        description: String,
        quantity_on_hand: usize,
        quantity_on_order: usize,
        projects_committed: BTreeMap<usize, usize>,
    }

    pub struct Inventory {
        parts: BTreeMap<usize, Part>,
        projects: BTreeMap<usize, Project>,
    }

    impl Inventory {
        pub fn query_parts_by_project_name<'a>(
            &'a self,
            project: &'a str,
        ) -> impl Iterator<Item = QueryResultRef<'a>> + 'a {
            self.parts.values().flat_map(move |part| {
                part.projects_committed
                    .iter()
                    .filter(move |&(proj, _)| self.projects[proj].name == project)
                    .map(move |(_, &quantity_committed)| QueryResultRef {
                        part_id: part.id,
                        part_name: &part.name,
                        quantity_committed,
                    })
            })
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            let mut result = Inventory {
                parts: BTreeMap::new(),
                projects: BTreeMap::new(),
            };
            //let proj_names: BTreeMap<usize, String> = projects.iter().map(|proj| (proj.id, proj.name.clone())).collect();

            result.parts.extend(parts.iter().map(|part| (part.id, Part {
                id: part.id,
                name: part.name.clone(),
                description: part.description.clone(),
                quantity_on_hand: part.quantity_on_hand,
                quantity_on_order: part.quantity_on_order,
                projects_committed: BTreeMap::new(),
            })));
            
            //result.projects.extend(projects.iter().map(|proj| (proj.name.clone(), Project {
            result.projects.extend(projects.iter().map(|proj| (proj.id, Project {
                id: proj.id,
                name: proj.name.clone(),
                description: proj.description.clone(),
            })));

            for commit in commits {
                //result.projects.get_mut(&proj_names[&commit.project_id]).unwrap()
                result.parts.get_mut(&commit.part_id).unwrap()
                      .projects_committed.insert(commit.project_id, commit.quantity_committed);
            }

            result
        }
    }
}

/// Parts and projects as peers
/// Commitment relationship subordinate to parts
pub mod struct4_alt {
    use super::QueryResultRef;
    use std::collections::BTreeMap;
    use crate::datagen as data;

    #[allow(dead_code)]
    struct Project {
        id: usize,
        name: String,
        description: String,
    }

    #[allow(dead_code)]
    struct Part {
        id: usize,
        name: String,
        description: String,
        quantity_on_hand: usize,
        quantity_on_order: usize,
        projects_committed: BTreeMap<String, usize>,
    }

    pub struct Inventory {
        parts: BTreeMap<usize, Part>,
        projects: BTreeMap<String, Project>,
    }

    impl Inventory {
        pub fn query_parts_by_project_name<'a>(
            &'a self,
            project: &'a str,
        ) -> impl Iterator<Item = QueryResultRef<'a>> + 'a {
            self.parts.values().flat_map(move |part| {
                part.projects_committed
                    .get(project)
                    .map(move |&quantity_committed| QueryResultRef {
                        part_id: part.id,
                        part_name: &part.name,
                        quantity_committed,
                    })
            })
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            let mut result = Inventory {
                parts: BTreeMap::new(),
                projects: BTreeMap::new(),
            };
            let proj_names: BTreeMap<usize, String> = projects.iter().map(|proj| (proj.id, proj.name.clone())).collect();

            result.parts.extend(parts.iter().map(|part| (part.id, Part {
                id: part.id,
                name: part.name.clone(),
                description: part.description.clone(),
                quantity_on_hand: part.quantity_on_hand,
                quantity_on_order: part.quantity_on_order,
                projects_committed: BTreeMap::new(),
            })));
            
            result.projects.extend(projects.iter().map(|proj| (proj.name.clone(), Project {
                id: proj.id,
                name: proj.name.clone(),
                description: proj.description.clone(),
            })));

            for commit in commits {
                result.parts.get_mut(&commit.part_id).unwrap()
                      .projects_committed.insert(proj_names[&commit.project_id].clone(), commit.quantity_committed);
            }

            result
        }
    }
}

/// Parts, projects, and commitment relationship as peers
pub mod struct5 {
    use super::QueryResultRef;
    use std::collections::BTreeMap;
    use crate::datagen as data;

    #[allow(dead_code)]
    struct Project {
        id: usize,
        name: String,
        description: String,
    }

    #[allow(dead_code)]
    struct Part {
        id: usize,
        name: String,
        description: String,
        quantity_on_hand: usize,
        quantity_on_order: usize,
    }

    #[allow(dead_code)]
    struct Commit {
        part_id: usize,
        project_id: usize,
        quantity_committed: usize,
    }

    pub struct Inventory {
        parts: BTreeMap<usize, Part>,
        projects: BTreeMap<usize, Project>,
        commitments: BTreeMap<(usize, usize), Commit>, // (part_id, project_id)
    }

    impl Inventory {
        pub fn query_parts_by_project_name<'a>(
            &'a self,
            project: &'a str,
        ) -> impl Iterator<Item = QueryResultRef<'a>> + 'a {
            self.projects
                .values()
                .filter(move |proj| proj.name == project)
                .flat_map(move |proj| {
                    self.commitments
                        .values()
                        .filter(move |commit| commit.project_id == proj.id)
                        .map(move |commit| QueryResultRef {
                            part_id: commit.part_id,
                            part_name: &self.parts[&commit.part_id].name,
                            quantity_committed: commit.quantity_committed,
                        })
                })
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            let mut result = Inventory {
                parts: BTreeMap::new(),
                projects: BTreeMap::new(),
                commitments: BTreeMap::new()
            };
            //let proj_names: BTreeMap<usize, String> = projects.iter().map(|proj| (proj.id, proj.name.clone())).collect();

            result.parts.extend(parts.iter().map(|part| (part.id, Part {
                id: part.id,
                name: part.name.clone(),
                description: part.description.clone(),
                quantity_on_hand: part.quantity_on_hand,
                quantity_on_order: part.quantity_on_order,
            })));
            
            //result.projects.extend(projects.iter().map(|proj| (proj.name.clone(), Project {
            result.projects.extend(projects.iter().map(|proj| (proj.id, Project {
                id: proj.id,
                name: proj.name.clone(),
                description: proj.description.clone(),
            })));

            result.commitments.extend(commits.iter().map(|commit| ((commit.part_id, commit.project_id), Commit {
                part_id: commit.part_id,
                project_id: commit.project_id,
                quantity_committed: commit.quantity_committed
            })));

            result
        }
    }
}

/// Parts, projects, and commitment relationship as peers
pub mod struct5_alt {
    use super::QueryResultRef;
    use std::collections::BTreeMap;
    use crate::datagen as data;

    #[allow(dead_code)]
    struct Project {
        id: usize,
        name: String,
        description: String,
    }

    #[allow(dead_code)]
    struct Part {
        id: usize,
        name: String,
        description: String,
        quantity_on_hand: usize,
        quantity_on_order: usize,
    }

    #[allow(dead_code)]
    struct Commit {
        part_id: usize,
        project_id: usize,
        quantity_committed: usize,
    }

    pub struct Inventory {
        parts: BTreeMap<usize, Part>,
        projects: BTreeMap<usize, Project>,
        commitments: BTreeMap<(String, usize), Commit>, // (project_name)
    }

    impl Inventory {
        pub fn query_parts_by_project_name<'a>(
            &'a self,
            project: &'a str,
        ) -> impl Iterator<Item = QueryResultRef<'a>> + 'a {
            self.commitments
                .range(&(String::from(project), 0)..=&(String::from(project), std::usize::MAX))
                .map(move |(_, commit)| QueryResultRef {
                    part_id: commit.part_id,
                    part_name: &self.parts[&commit.part_id].name,
                    quantity_committed: commit.quantity_committed,
                })
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            let mut result = Inventory {
                parts: BTreeMap::new(),
                projects: BTreeMap::new(),
                commitments: BTreeMap::new()
            };
            let proj_names: BTreeMap<usize, String> = projects.iter().map(|proj| (proj.id, proj.name.clone())).collect();

            result.parts.extend(parts.iter().map(|part| (part.id, Part {
                id: part.id,
                name: part.name.clone(),
                description: part.description.clone(),
                quantity_on_hand: part.quantity_on_hand,
                quantity_on_order: part.quantity_on_order,
            })));
            
            result.projects.extend(projects.iter().map(|proj| (proj.id, Project {
                id: proj.id,
                name: proj.name.clone(),
                description: proj.description.clone(),
            })));

            result.commitments.extend(commits.iter().map(|commit| ((proj_names[&commit.project_id].clone(), commit.part_id), Commit {
                part_id: commit.part_id,
                project_id: commit.project_id,
                quantity_committed: commit.quantity_committed
            })));

            result
        }
    }
}
