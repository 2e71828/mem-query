use ::memquery as mq;
use mq::{
    prelude::*,
    tylisp::{sexpr, sexpr_val},
    relation::{SelfQuery, BTreeIndex, Insert, Queryable, RelProxy, RelationImpl},
    query::{filter::Exact, request::{BlankRequest, QueryRequest}},
    record::{FromRecordImpl, RecordImpl},
    header::HasCol,
};
use crate::{QueryResultRef,QueryResult};
use crate::datagen as data;

mq::col!{ pub PartId: usize }
mq::col!{ pub PartName: String }
mq::col!{ pub PartDesc: String }
mq::col!{ pub QtyOnHand: usize }
mq::col!{ pub QtyOnOrder: usize }
mq::col!{ pub ProjectId: usize }
mq::col!{ pub ProjectName: String }
mq::col!{ pub ProjectDesc: String }
mq::col!{ pub QtyCommitted: usize }

impl FromRecordImpl for QueryResult {
    type Cols = <(PartId, (PartName, QtyCommitted)) as FromRecordImpl>::Cols;
    fn from_rec(r: impl Record<Cols=Self::Cols>)->Self {
        let (PartId(part_id), (PartName(part_name), QtyCommitted(quantity_committed))) = r.project_into();
        QueryResult { part_id, part_name, quantity_committed }
    }
}

impl RecordImpl for data::Project {
    type Cols = sexpr!{ProjectId, ProjectName, ProjectDesc};

    fn into_cols(self)->Self::Cols {
        sexpr_val!{ProjectId(self.id), ProjectName(self.name), ProjectDesc(self.description)}
    }

    fn clone_cols(&self)->Self::Cols {
        sexpr_val!{ProjectId(self.id), ProjectName(self.name.clone()), ProjectDesc(self.description.clone())}
    }

    fn col_ref<C:Col>(&self)->&C where Self::Cols: HasCol<C> { self.col_opt().unwrap() }

    fn col_opt<C:Col>(&self)->Option<&C> {
        use std::any::Any;
        if let Some(c) = (ProjectId::wrap_ref(&self.id) as &dyn Any).downcast_ref() { return Some(c); }
        if let Some(c) = (ProjectName::wrap_ref(&self.name) as &dyn Any).downcast_ref() { return Some(c); }
        if let Some(c) = (ProjectDesc::wrap_ref(&self.description) as &dyn Any).downcast_ref() { return Some(c); }
        None
    }
}
 
impl RecordImpl for data::Part {
    type Cols = sexpr!{ PartId, PartName, PartDesc, QtyOnHand, QtyOnOrder };

    fn into_cols(self)->Self::Cols {
        sexpr_val!{
            PartId(self.id),
            PartName(self.name),
            PartDesc(self.description),
            QtyOnHand(self.quantity_on_hand),
            QtyOnOrder(self.quantity_on_order)
        }
    }

    fn clone_cols(&self)->Self::Cols {
        sexpr_val!{
            PartId(self.id),
            PartName(self.name.clone()),
            PartDesc(self.description.clone()),
            QtyOnHand(self.quantity_on_hand),
            QtyOnOrder(self.quantity_on_order)
        }
    }

    fn col_ref<C:Col>(&self)->&C where Self::Cols: HasCol<C> { self.col_opt().unwrap() }

    fn col_opt<C:Col>(&self)->Option<&C> {
        use std::any::Any;
        if let Some(c) = (PartId::wrap_ref(&self.id) as &dyn Any).downcast_ref() { return Some(c); }
        if let Some(c) = (PartName::wrap_ref(&self.name) as &dyn Any).downcast_ref() { return Some(c); }
        if let Some(c) = (PartDesc::wrap_ref(&self.description) as &dyn Any).downcast_ref() { return Some(c); }
        if let Some(c) = (QtyOnHand::wrap_ref(&self.quantity_on_hand) as &dyn Any).downcast_ref() { return Some(c); }
        if let Some(c) = (QtyOnOrder::wrap_ref(&self.quantity_on_order) as &dyn Any).downcast_ref() { return Some(c); }
        None
    }
}

type ReqType = mq::query::request::AddFilter<mq::query::request::WithCols<memquery::query::request::BlankRequest, sexpr!{PartId, PartName, QtyCommitted, ProjectName}>, memquery::query::filter::Exact<ProjectName>>;
fn make_request(project: &str)->ReqType {
    BlankRequest.set_cols::<sexpr!{PartId,PartName,QtyCommitted,ProjectName}>()
                .add_filter(Exact(ProjectName(String::from(project))))
}

pub mod struct1 {
    use super::*;
    use std::rc::Rc;

    mq::col!{ PartProjects: RelProxy<Rc<BTreeIndex<ProjectId, Option<(ProjectName, (ProjectDesc, QtyCommitted))>>>> }

    #[derive(Default)]
    pub struct Inventory {
        parts: BTreeIndex<PartId, Option<(PartName, (PartDesc, (QtyOnHand, (QtyOnOrder, PartProjects))))>>,
    }

    impl Inventory {
        pub fn query_parts_by_project_name(
            &self,
            project: &str,
            mut out: Box<dyn FnMut(QueryResult)>
        ) {
            self.parts.as_ref().subjoin::<PartProjects>()
                .where_eq(ProjectName(String::from(project)))
                .iter_as::<QueryResult>()
                .for_each(out)
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            use std::collections::BTreeMap;
            let projects: BTreeMap<usize, &data::Project> = projects.iter().map(|proj| (proj.id, proj)).collect();
            let mut commits_by_part: BTreeMap<usize, Vec<&data::Commit>> = Default::default();
            for commit in commits {
                commits_by_part.entry(commit.part_id).or_default().push(commit);
            }

            let mut inv: Self = Default::default();
            inv.parts.insert_multi(parts.iter().map(|part| {
                let mut part_projects:BTreeIndex<_,_> = Default::default();
                part_projects.insert_multi(
                    commits_by_part.entry(part.id).or_default().iter().map(|&data::Commit{ project_id, quantity_committed, part_id: _ }| {
                        (projects[project_id].clone(), QtyCommitted(*quantity_committed))
                    })
                ).unwrap();
                (part, PartProjects(RelProxy::new(part_projects)))
            })).unwrap();
            println!("struct1 plan: {}", Queryable::<'_, ReqType>::explain(&inv.parts.as_ref().subjoin::<PartProjects>()));
            inv
        }
    }
}

pub mod struct1_alt {
    use super::*;
    use std::rc::Rc;

    mq::col!{ PartProjects: RelProxy<Rc<BTreeIndex<ProjectName, Option<(ProjectId, (ProjectDesc, QtyCommitted))>>>> }

    #[derive(Default)]
    pub struct Inventory {
        parts: BTreeIndex<PartId, Option<(PartName, (PartDesc, (QtyOnHand, (QtyOnOrder, PartProjects))))>>,
    }

    impl Inventory {
        pub fn query_parts_by_project_name(
            &self,
            project: &str,
            mut out: Box<dyn FnMut(QueryResultRef)>
        ) {
            self.parts.as_ref().subjoin::<PartProjects>()
//                .iter_all().filter(|r| r.col_ref::<ProjectName>().as_str() == project)
//                .query(BlankRequest.set_cols::<sexpr!{PartId,PartName,QtyCommitted,ProjectName}>()
//                                         .add_filter(Exact(ProjectName(String::from(project)))))
                .where_eq(ProjectName(String::from(project)))
                .iter_all()
                .for_each(|r| out(QueryResultRef {
                    part_id: **(r.col_ref::<PartId>()),
                    part_name: r.col_ref::<PartName>().as_str(),
                    quantity_committed: **(r.col_ref::<QtyCommitted>())
                }));
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            use std::collections::BTreeMap;
            let projects: BTreeMap<usize, &data::Project> = projects.iter().map(|proj| (proj.id, proj)).collect();
            let mut commits_by_part: BTreeMap<usize, Vec<&data::Commit>> = Default::default();
            for commit in commits {
                commits_by_part.entry(commit.part_id).or_default().push(commit);
            }

            let mut inv: Self = Default::default();
            inv.parts.insert_multi(parts.iter().map(|part| {
                let mut part_projects:BTreeIndex<_,_> = Default::default();
                part_projects.insert_multi(
                    commits_by_part.entry(part.id).or_default().iter().map(|&data::Commit{ project_id, quantity_committed, part_id: _ }| {
                        let proj = projects[&project_id];
                        sexpr_val!{
                            QtyCommitted(*quantity_committed),
                            ProjectId(proj.id),
                            ProjectName(proj.name.clone()),
                            ProjectDesc(proj.description.clone())
                        }
                    })
                ).unwrap();
                sexpr_val!{
                    PartId(part.id),
                    PartProjects(RelProxy::new(part_projects)),
                    PartName(part.name.clone()),
                    PartDesc(part.description.clone()),
                    QtyOnHand(part.quantity_on_hand),
                    QtyOnOrder(part.quantity_on_order),
                    
                }
            })).unwrap();

            println!("struct1_alt plan: {}", Queryable::<'_, ReqType>::explain(&inv.parts.as_ref().subjoin::<PartProjects>()));
            inv
        }
    }
}

pub mod struct2 {
    use super::*;
    use std::rc::Rc;

    mq::col!{ ProjectParts: RelProxy<Rc<BTreeIndex<PartId, Option<(PartName, (PartDesc, (QtyOnHand, (QtyOnOrder, QtyCommitted))))>>>> }

    #[derive(Default)]
    pub struct Inventory {
        projects: BTreeIndex<ProjectId, Option<(ProjectName, (ProjectDesc, ProjectParts))>>,
    }

    impl Inventory {
        pub fn query_parts_by_project_name(
            &self,
            project: &str,
            mut out: Box<dyn FnMut(QueryResultRef)>
        ) {
            self.projects.as_ref().subjoin::<ProjectParts>()
//                .iter_all().filter(|r| r.col_ref::<ProjectName>().as_str() == project)
//                .query(BlankRequest.set_cols::<sexpr!{PartId,PartName,QtyCommitted,ProjectName}>()
//                                         .add_filter(Exact(ProjectName(String::from(project)))))
                .where_eq(ProjectName(String::from(project)))
                .iter_all()
                .for_each(|r| out(QueryResultRef {
                    part_id: **(r.col_ref::<PartId>()),
                    part_name: r.col_ref::<PartName>().as_str(),
                    quantity_committed: **(r.col_ref::<QtyCommitted>())
                }));
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            use std::collections::BTreeMap;
            let parts: BTreeMap<usize, &data::Part> = parts.iter().map(|part| (part.id, part)).collect();
            let mut commits_by_project: BTreeMap<usize, Vec<&data::Commit>> = Default::default();
            for commit in commits {
                commits_by_project.entry(commit.project_id).or_default().push(commit);
            }

            let mut inv: Self = Default::default();
            inv.projects.insert_multi(projects.iter().map(|proj| {
                let mut project_parts:BTreeIndex<_,_> = Default::default();
                project_parts.insert_multi(
                    commits_by_project.entry(proj.id).or_default().iter().map(|&data::Commit{ project_id: _, quantity_committed, part_id }| {
                        let part = parts[&part_id];
                        sexpr_val!{
                            PartId(part.id),
                            PartName(part.name.clone()),
                            PartDesc(part.description.clone()),
                            QtyOnHand(part.quantity_on_hand),
                            QtyOnOrder(part.quantity_on_order),
                            QtyCommitted(*quantity_committed),
                        }
                    })
                ).unwrap();
                sexpr_val!{
                    ProjectId(proj.id),
                    ProjectName(proj.name.clone()),
                    ProjectDesc(proj.description.clone()),
                    ProjectParts(RelProxy::new(project_parts)),
                }
            })).unwrap();

            println!("struct2 plan: {}", Queryable::<'_, ReqType>::explain(&inv.projects.as_ref().subjoin::<ProjectParts>()));
            inv
        }
    }
}

pub mod struct2_alt {
    use super::*;
    use std::rc::Rc;

    mq::col!{ ProjectParts: RelProxy<Rc<BTreeIndex<PartId, Option<(PartName, (PartDesc, (QtyOnHand, (QtyOnOrder, QtyCommitted))))>>>> }

    #[derive(Default)]
    pub struct Inventory {
        projects: BTreeIndex<ProjectName, Option<(ProjectId, (ProjectDesc, ProjectParts))>>,
    }

    impl Inventory {
        pub fn query_parts_by_project_name(
            &self,
            project: &str,
            mut out: Box<dyn FnMut(QueryResultRef)>
        ) {
            self.projects.as_ref().subjoin::<ProjectParts>()
//                .iter_all().filter(|r| r.col_ref::<ProjectName>().as_str() == project)
//                .query(BlankRequest.set_cols::<sexpr!{PartId,PartName,QtyCommitted,ProjectName}>()
//                                         .add_filter(Exact(ProjectName(String::from(project)))))
                .where_eq(ProjectName(String::from(project)))
                .iter_all()
                .for_each(|r| out(QueryResultRef {
                    part_id: **(r.col_ref::<PartId>()),
                    part_name: r.col_ref::<PartName>().as_str(),
                    quantity_committed: **(r.col_ref::<QtyCommitted>())
                }));
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            use std::collections::BTreeMap;
            let parts: BTreeMap<usize, &data::Part> = parts.iter().map(|part| (part.id, part)).collect();
            let mut commits_by_project: BTreeMap<usize, Vec<&data::Commit>> = Default::default();
            for commit in commits {
                commits_by_project.entry(commit.project_id).or_default().push(commit);
            }

            let mut inv: Self = Default::default();
            inv.projects.insert_multi(projects.iter().map(|proj| {
                let mut project_parts:BTreeIndex<_,_> = Default::default();
                project_parts.insert_multi(
                    commits_by_project.entry(proj.id).or_default().iter().map(|&data::Commit{ project_id: _, quantity_committed, part_id }| {
                        let part = parts[&part_id];
                        sexpr_val!{
                            PartId(part.id),
                            PartName(part.name.clone()),
                            PartDesc(part.description.clone()),
                            QtyOnHand(part.quantity_on_hand),
                            QtyOnOrder(part.quantity_on_order),
                            QtyCommitted(*quantity_committed),
                        }
                    })
                ).unwrap();
                sexpr_val!{
                    ProjectId(proj.id),
                    ProjectName(proj.name.clone()),
                    ProjectDesc(proj.description.clone()),
                    ProjectParts(RelProxy::new(project_parts)),
                }
            })).unwrap();

            println!("struct2_alt plan: {}", Queryable::<'_, ReqType>::explain(&inv.projects.as_ref().subjoin::<ProjectParts>()));
            inv
        }
    }
}

/// Parts and projects as peers
/// Commitment relationship subordinate to projects
pub mod struct3 {
    use super::*;
    use std::rc::Rc;

    mq::col!{ ProjCommits: RelProxy<Rc<BTreeIndex<PartId, Option<QtyCommitted>>>> }

    #[derive(Default)]
    pub struct Inventory {
        parts: BTreeIndex<PartId, Option<sexpr!{PartName, PartDesc, QtyOnHand, QtyOnOrder}>>,
        projects: BTreeIndex<ProjectId, Option<sexpr!{ProjectName, ProjectDesc, ProjCommits}>>,
    }

    impl Inventory {
        pub fn query_parts_by_project_name(
            &self,
            project: &str,
            mut out: Box<dyn FnMut(QueryResult)>
        ) {
            self.projects.as_ref()
                .subjoin::<ProjCommits>()
                .join(self.parts.as_ref())
                .where_eq(ProjectName(String::from(project)))
                .iter_as::<QueryResult>()
                .for_each(out)
        }

        pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
            use std::collections::BTreeMap;
            let mut commits_by_proj: BTreeMap<usize, Vec<(usize,usize)>> = Default::default();
            for commit in commits {
                commits_by_proj.entry(commit.project_id).or_default().push((commit.part_id, commit.quantity_committed));
            }

            let mut inv: Self = Default::default();
            inv.parts.insert_multi(parts.iter().cloned());
            inv.projects.insert_multi(projects.iter().map(|proj| {
                let mut proj_commits:BTreeIndex<_,_> = Default::default();
                proj_commits.insert_multi(
                    commits_by_proj
                        .entry(proj.id)
                        .or_default()
                        .iter().map(|&(p,q)| (PartId(p), QtyCommitted(q)))
                ).unwrap();
                (proj, ProjCommits(RelProxy::new(proj_commits)))
            })).unwrap();
            println!("struct3 plan: {}", Queryable::<'_, ReqType>::explain(&inv.projects.as_ref().subjoin::<ProjCommits>().join(inv.parts.as_ref())));
            inv
        }
    }
}
