use datagen::QueryResultRef;
use datagen as data;
use std::collections::BTreeMap;

#[allow(dead_code)]
struct Project {
    id: usize,
    name: String,
    description: String,
}

#[allow(dead_code)]
struct Part {
    id: usize,
    name: String,
    description: String,
    quantity_on_hand: usize,
    quantity_on_order: usize,
    projects_committed: BTreeMap<usize, usize>,
}

pub struct Inventory {
    parts: BTreeMap<usize, Part>,
    projects: BTreeMap<usize, Project>,
}

impl Inventory {
    pub fn query_parts_by_project_name<'a>(
        &'a self,
        project: &'a str,
        out: &mut dyn Fn(QueryResultRef),
    ) {
        self.parts.values().for_each(|part| {
            part.projects_committed
                .iter()
                .filter(|&(proj, _)| self.projects[proj].name == project)
                .for_each(|(_, &quantity_committed)| out(QueryResultRef {
                    part_id: part.id,
                    part_name: &part.name,
                    quantity_committed,
                }))
        })
    }

    pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
        let mut result = Inventory {
            parts: BTreeMap::new(),
            projects: BTreeMap::new(),
        };
        //let proj_names: BTreeMap<usize, String> = projects.iter().map(|proj| (proj.id, proj.name.clone())).collect();

        result.parts.extend(parts.iter().map(|part| (part.id, Part {
            id: part.id,
            name: part.name.clone(),
            description: part.description.clone(),
            quantity_on_hand: part.quantity_on_hand,
            quantity_on_order: part.quantity_on_order,
            projects_committed: BTreeMap::new(),
        })));
        
        //result.projects.extend(projects.iter().map(|proj| (proj.name.clone(), Project {
        result.projects.extend(projects.iter().map(|proj| (proj.id, Project {
            id: proj.id,
            name: proj.name.clone(),
            description: proj.description.clone(),
        })));

        for commit in commits {
            //result.projects.get_mut(&proj_names[&commit.project_id]).unwrap()
            result.parts.get_mut(&commit.part_id).unwrap()
                  .projects_committed.insert(commit.project_id, commit.quantity_committed);
        }

        result
    }
}
