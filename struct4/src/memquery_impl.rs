use ::mem_query as mq;
use mq::{
    prelude::*,
    relation::{BTreeIndex, Insert, RelProxy, RelationImpl},
};
use ::datagen::*;

use std::rc::Rc;

mq::col!{ PartCommits: RelProxy<Rc<BTreeIndex<ProjectId, Option<(ProjectId,QtyCommitted)>>>> }

#[derive(Default)]
pub struct Inventory {
    parts: BTreeIndex<PartId, Option<(Part, PartCommits)>>,
    projects: BTreeIndex<ProjectId, Option<Project>>,
}

impl Inventory {
    pub fn query_parts_by_project_name(
        &self,
        project: &str,
        out: &mut dyn FnMut(QueryResultRef)
    ) {
        self.parts.as_ref()
            .subjoin::<PartCommits>()
            .join(self.projects.as_ref())
            .where_eq(&ProjectName(String::from(project)))
            .iter_as::<QueryResultRef>()
            .for_each(out)
    }

    pub fn load(&(ref projects, ref parts, ref commits): &(Vec<Project>, Vec<Part>, Vec<Commit>))->Self {
        use std::collections::BTreeMap;
        let mut commits_by_part: BTreeMap<usize, Vec<(usize,usize)>> = Default::default();
        for commit in commits {
            commits_by_part.entry(commit.part_id).or_default().push((commit.project_id, commit.quantity_committed));
        }

        let mut inv: Self = Default::default();
        inv.projects.insert_multi(projects.iter().cloned()).unwrap();
        inv.parts.insert_multi(parts.iter().map(|part| {
            let mut part_commits:BTreeIndex<_,_> = Default::default();
            part_commits.insert_multi(
                commits_by_part
                    .entry(part.id)
                    .or_default()
                    .iter().map(|&(p,q)| (ProjectId(p), QtyCommitted(q)))
            ).unwrap();
            (part, PartCommits(RelProxy::new(part_commits)))
        })).unwrap();
        inv
    }
}

