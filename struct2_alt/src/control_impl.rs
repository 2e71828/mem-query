use datagen::QueryResultRef;
use datagen as data;
use std::collections::BTreeMap;


#[allow(dead_code)]
struct Project {
    id: usize,
    name: String,
    description: String,
    parts: BTreeMap<usize, Part>,
}

#[allow(dead_code)]
struct Part {
    id: usize,
    name: String,
    description: String,
    quantity_on_hand: usize,
    quantity_on_order: usize,
    quantity_committed: usize,
}

pub struct Inventory {
    projects: BTreeMap<String, Project>,
}

impl Inventory {
    pub fn query_parts_by_project_name(
        &self,
        project: &str,
        out: &mut dyn FnMut(QueryResultRef)
    ) {
        self.projects
            .get(project)
            .map(move |proj| {
                proj.parts.values().for_each(|part| out(QueryResultRef {
                    part_id: part.id,
                    part_name: &part.name,
                    quantity_committed: part.quantity_committed,
                }));
            });
    }

    pub fn load(&(ref projects, ref parts, ref commits): &(Vec<data::Project>, Vec<data::Part>, Vec<data::Commit>))->Self {
        let mut result = Inventory { projects: BTreeMap::new() };
        let mut project_names: BTreeMap<usize, String> = BTreeMap::new();

        for proj in projects {
            result.projects.insert(proj.name.clone(), Project {
                id: proj.id,
                name: proj.name.clone(),
                description: proj.description.clone(),
                parts: BTreeMap::new()
            });

            project_names.insert(proj.id, proj.name.clone());
        }

        let parts: BTreeMap<usize, &data::Part> = parts.iter().map(|part| (part.id, part)).collect();

        for commit in commits {
            let part = parts[&commit.part_id];
            result.projects.get_mut(&project_names[&commit.project_id]).unwrap().parts.insert(part.id, Part {
                id: part.id,
                name: part.name.clone(),
                description: part.description.clone(),
                quantity_on_hand: part.quantity_on_hand,
                quantity_on_order: part.quantity_on_order,
                quantity_committed: commit.quantity_committed
            });
        }
        result
    }
}
