use ::mem_query as mq;
use mq::{
    prelude::*,
    relation::{BTreeIndex, Insert, RelProxy, RelationImpl},
};
use ::datagen::*;

use std::rc::Rc;

mq::col!{ ProjectParts: RelProxy<Rc<BTreeIndex<PartId, Option<(Part, QtyCommitted)>>>> }

#[derive(Default)]
pub struct Inventory {
    projects: BTreeIndex<ProjectName, Option<(Project, ProjectParts)>>,
}

impl Inventory {
    pub fn query_parts_by_project_name(
        &self,
        project: &str,
        out: &mut dyn FnMut(QueryResultRef)
    ) {
        self.projects.as_ref().subjoin::<ProjectParts>()
            .where_eq(&ProjectName(String::from(project)))
            .iter_as::<QueryResultRef>()
            .for_each(out)
    }

    pub fn load(&(ref projects, ref parts, ref commits): &(Vec<Project>, Vec<Part>, Vec<Commit>))->Self {
        use std::collections::BTreeMap;
        let parts: BTreeMap<usize, &Part> = parts.iter().map(|part| (part.id, part)).collect();
        let mut commits_by_project: BTreeMap<usize, Vec<&Commit>> = Default::default();
        for commit in commits {
            commits_by_project.entry(commit.project_id).or_default().push(commit);
        }

        let mut inv: Self = Default::default();
        inv.projects.insert_multi(projects.iter().map(|proj| {
            let mut project_parts:BTreeIndex<_,_> = Default::default();
            project_parts.insert_multi(
                commits_by_project.entry(proj.id).or_default().iter().map(|&Commit{ project_id: _, quantity_committed, part_id }| {
                    let part = parts[&part_id];
                    (
                        part.clone(), 
                        QtyCommitted(*quantity_committed),
                    )
                })
            ).unwrap();
            (
                proj.clone(),
                ProjectParts(RelProxy::new(project_parts)),
            )
        })).unwrap();
        inv
    }
}
