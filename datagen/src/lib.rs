#![recursion_limit = "512"]

use rand_chacha;
use rand;
use rand::{Rng, seq::SliceRandom};
use std::collections::BTreeMap;

#[derive(Clone, Debug)]
pub struct Project {
    pub id: usize,
    pub name: String,
    pub description: String,
}

#[derive(Clone, Debug)]
pub struct Part {
    pub id: usize,
    pub name: String,
    pub description: String,
    pub quantity_on_hand: usize,
    pub quantity_on_order: usize,
}

#[derive(Clone, Debug)]
pub struct Commit {
    pub part_id: usize,
    pub project_id: usize,
    pub quantity_committed: usize,
}

#[derive(Debug)]
pub struct QueryResult {
    pub part_id: usize,
    pub part_name: String,
    pub quantity_committed: usize,
}

#[derive(Debug)]
pub struct QueryResultRef<'a> {
    pub part_id: usize,
    pub part_name: &'a str,
    pub quantity_committed: usize,
}

use ::mem_query as mq;
use mq::{
    prelude::*,
    tylisp::{sexpr, sexpr_val, sexpr_pat},
    record::{FromRecordImpl, Record, ExternalRecord, FromExternalRecord},
    header::HasCol,
};

mq::col!{ pub PartId: usize }
mq::col!{ pub PartName: String }
// mq::col!{ pub PartDesc: String }
// mq::col!{ pub QtyOnHand: usize }
// mq::col!{ pub QtyOnOrder: usize }
mq::col!{ pub ProjectId: usize }
mq::col!{ pub ProjectName: String }
// mq::col!{ pub ProjectDesc: String }
mq::col!{ pub QtyCommitted: usize }

impl FromRecordImpl for QueryResult {
    type Cols = sexpr!{PartId, PartName, QtyCommitted};
    fn from_rec_raw(r: impl Record<Cols=Self::Cols>)->Self {
        let sexpr_pat!{part_id:_, part_name:_, quantity_committed:_} = r.into_cols();
        QueryResult {
            part_id: part_id.0,
            part_name: part_name.0,
            quantity_committed: quantity_committed.0
        }
    }
}

impl<'a> FromExternalRecord<'a> for QueryResult {
    type Cols = <Self as FromRecordImpl>::Cols;
    fn from_ext_rec_raw(r: impl ExternalRecord<'a, Cols=Self::Cols>)->Self {
        Self::from_rec_raw(r)
    }
}

impl<'a> FromExternalRecord<'a> for QueryResultRef<'a> {
    type Cols = sexpr!{PartId, PartName, QtyCommitted};
    fn from_ext_rec_raw(r: impl ExternalRecord<'a, Cols=Self::Cols>)->Self {
        QueryResultRef {
            part_id: r.ext_col_ref::<PartId>().0,
            part_name: r.ext_col_ref::<PartName>().as_str(),
            quantity_committed: r.ext_col_ref::<QtyCommitted>().0,
        }
    }
}

impl Record for Project {
    type Cols = sexpr!{ProjectId, ProjectName/*, ProjectDesc*/};

    fn into_cols(self)->Self::Cols {
        sexpr_val!{
            ProjectId(self.id),
            ProjectName(self.name),
            // ProjectDesc(self.description)
        }
    }

    fn clone_cols(&self)->Self::Cols {
        sexpr_val!{
            ProjectId(self.id),
            ProjectName(self.name.clone()),
            // ProjectDesc(self.description.clone())
        }
    }

    fn col_ref<C:Col>(&self)->&C where Self::Cols: HasCol<C> { self.col_opt().unwrap() }

    fn col_opt<C:Col>(&self)->Option<&C> {
        use std::any::Any;
        if let Some(c) = (ProjectId::wrap_ref(&self.id) as &dyn Any).downcast_ref() { return Some(c); }
        if let Some(c) = (ProjectName::wrap_ref(&self.name) as &dyn Any).downcast_ref() { return Some(c); }
//        if let Some(c) = (ProjectDesc::wrap_ref(&self.description) as &dyn Any).downcast_ref() { return Some(c); }
        None
    }
}

impl FromRecordImpl for Project {
    type Cols = sexpr!{ProjectId, ProjectName/*, ProjectDesc*/};
    fn from_rec_raw(r: impl Record<Cols=Self::Cols>)->Self {
        let sexpr_pat!{id:_, name:_/*, description:_*/} = r.into_cols();
        Project {
            id: id.0,
            name: name.0,
            description: Default::default() // description.0
        }
    }
}

impl Record for Part {
    type Cols = sexpr!{ PartId, PartName /*, PartDesc, QtyOnHand, QtyOnOrder */ };

    fn into_cols(self)->Self::Cols {
        sexpr_val!{
            PartId(self.id),
            PartName(self.name),
            // PartDesc(self.description),
            // QtyOnHand(self.quantity_on_hand),
            // QtyOnOrder(self.quantity_on_order)
        }
    }

    fn clone_cols(&self)->Self::Cols {
        sexpr_val!{
            PartId(self.id),
            PartName(self.name.clone()),
            // PartDesc(self.description.clone()),
            // QtyOnHand(self.quantity_on_hand),
            // QtyOnOrder(self.quantity_on_order)
        }
    }

    fn col_ref<C:Col>(&self)->&C where Self::Cols: HasCol<C> { self.col_opt().unwrap() }

    fn col_opt<C:Col>(&self)->Option<&C> {
        use std::any::Any;
        if let Some(c) = (PartId::wrap_ref(&self.id) as &dyn Any).downcast_ref() { return Some(c); }
        if let Some(c) = (PartName::wrap_ref(&self.name) as &dyn Any).downcast_ref() { return Some(c); }
        // if let Some(c) = (PartDesc::wrap_ref(&self.description) as &dyn Any).downcast_ref() { return Some(c); }
        // if let Some(c) = (QtyOnHand::wrap_ref(&self.quantity_on_hand) as &dyn Any).downcast_ref() { return Some(c); }
        // if let Some(c) = (QtyOnOrder::wrap_ref(&self.quantity_on_order) as &dyn Any).downcast_ref() { return Some(c); }
        None
    }
}

impl FromRecordImpl for Part {
    type Cols = sexpr!{ PartId, PartName/*, PartDesc, QtyOnHand, QtyOnOrder */ };
    fn from_rec_raw(r: impl Record<Cols=Self::Cols>)->Self {
        let sexpr_pat!{
            id:_,
            name:_,
            //description:_,
            //quantity_on_hand:_,
            //quantity_on_order:_
        } = r.into_cols();
        Part {
            id: id.0,
            name: name.0,
            description: Default::default(), //description.0,
            quantity_on_hand: 0, //quantity_on_hand.0,
            quantity_on_order: 0, //quantity_on_order.0
        }
    }
}

impl Record for Commit {
    type Cols = sexpr!{PartId, ProjectId, QtyCommitted};
    fn into_cols(self)->Self::Cols {
        sexpr_val!{
            PartId(self.part_id),
            ProjectId(self.project_id),
            QtyCommitted(self.quantity_committed)
        }
    }

    fn clone_cols(&self)->Self::Cols {
        sexpr_val!{
            PartId(self.part_id),
            ProjectId(self.project_id),
            QtyCommitted(self.quantity_committed)
        }
    }

    fn col_ref<C:Col>(&self)->&C where Self::Cols: HasCol<C> { self.col_opt().unwrap() }

    fn col_opt<C:Col>(&self)->Option<&C> {
        use std::any::Any;
        if let Some(c) = (PartId::wrap_ref(&self.part_id) as &dyn Any).downcast_ref() { return Some(c); }
        if let Some(c) = (ProjectId::wrap_ref(&self.project_id) as &dyn Any).downcast_ref() { return Some(c); }
        if let Some(c) = (QtyCommitted::wrap_ref(&self.quantity_committed) as &dyn Any).downcast_ref() { return Some(c); }
        None
    }
}

impl FromRecordImpl for Commit {
    type Cols = sexpr!{ PartId, ProjectId, QtyCommitted };
    fn from_rec_raw(r: impl Record<Cols=Self::Cols>)->Self {
        Commit {
            part_id: r.col_ref::<PartId>().0,
            project_id: r.col_ref::<ProjectId>().0,
            quantity_committed: r.col_ref::<QtyCommitted>().0
        }
    }
}
pub fn new_rng()->impl rand::Rng {
    // First 256 bits of pi, courtesy OEIS Seq. A062964
    const SEED:[u8;32] = [
        0x32, 0x43, 0xF6, 0xA8, 0x88, 0x5A, 0x30, 0x8D,
        0x31, 0x31, 0x98, 0xA2, 0xE0, 0x37, 0x07, 0x34,
        0x4A, 0x40, 0x93, 0x82, 0x22, 0x99, 0xF3, 0x1D,
        0x00, 0x82, 0xEF, 0xA9, 0x8E, 0xC4, 0xE6, 0xC8,
    ];

    <rand_chacha::ChaCha12Rng as rand::SeedableRng>::from_seed(SEED)
}

pub fn make_data(n_proj:usize, n_part:usize, n_commit:usize, n_queries:usize)
    ->((Vec<Project>, Vec<Part>, Vec<Commit>), Vec<String>) {
    let mut rng = new_rng();

    let mut projects: Vec<Project> = (0..n_proj).map(|id|
        Project {
            id,
            name: format!("proj-{:06}", id),
            description: format!("Project #{}", id)
        }
    ).collect();

    let mut parts: Vec<Part> = (0..n_part).map(|id|
        Part {
            id,
            name: format!("part-{:06}", id),
            description: format!("Part #{}", id),
            quantity_on_hand: rng.gen_range(0..100000),
            quantity_on_order: rng.gen_range(0..10000),
        }
    ).collect();

    let mut commits: BTreeMap<(usize,usize),usize> = BTreeMap::new();

    for _ in 0..n_commit {
        let proj = rng.gen_range(0..n_proj);
        let part = rng.gen_range(0..n_part);

        *commits.entry((proj,part)).or_default() += rng.gen_range(1..1000);
    }

    let mut commits: Vec<Commit> = commits.into_iter()
        .map(|((project_id, part_id), quantity_committed)| Commit {
            project_id, part_id, quantity_committed
        }).collect();

    projects.shuffle(&mut rng);
    parts.shuffle(&mut rng);
    commits.shuffle(&mut rng);
    
    let queries: Vec<String> = projects
        .choose_multiple(&mut rng, n_queries)
        .map(|proj| proj.name.clone())
        .collect();

    ((projects, parts, commits), queries)
} 
