use ::mem_query as mq;
use mq::{
    prelude::*,
    relation::{BTreeIndex, Insert, RelProxy, RelationImpl},
};
use ::datagen::*;

use std::rc::Rc;

mq::col!{ ProjCommits: RelProxy<Rc<
    BTreeIndex<PartId, Option<(PartId, QtyCommitted)>>
>> }

#[derive(Default)]
pub struct Inventory {
    parts: BTreeIndex<PartId, Option<Part>>,
    projects: BTreeIndex<ProjectId, Option<(Project, ProjCommits)>>,
}

impl Inventory {
    pub fn query_parts_by_project_name(
        &self,
        project: &str,
        out: &mut dyn FnMut(QueryResultRef)
    ) {
        self.projects.as_ref()
            .subjoin::<ProjCommits>()
            .join(self.parts.as_ref())
            .where_eq(&ProjectName(String::from(project)))
            .iter_as::<QueryResultRef>()
            .for_each(out)
    }

    pub fn load(&(ref projects, ref parts, ref commits): &(Vec<Project>, Vec<Part>, Vec<Commit>))->Self {
        use std::collections::BTreeMap;
        let mut commits_by_proj: BTreeMap<usize, Vec<(usize,usize)>> = Default::default();
        for commit in commits {
            commits_by_proj.entry(commit.project_id).or_default().push((commit.part_id, commit.quantity_committed));
        }

        let mut inv: Self = Default::default();
        inv.parts.insert_multi(parts.iter().cloned()).unwrap();
        inv.projects.insert_multi(projects.iter().map(|proj| {
            let mut proj_commits:BTreeIndex<_,_> = Default::default();
            proj_commits.insert_multi(
                commits_by_proj
                    .entry(proj.id)
                    .or_default()
                    .iter().map(|&(p,q)| (PartId(p), QtyCommitted(q)))
            ).unwrap();
            (proj, ProjCommits(RelProxy::new(proj_commits)))
        })).unwrap();
        inv
    }
}

